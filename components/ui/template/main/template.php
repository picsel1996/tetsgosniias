<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 23.06.2019
 * Time: 17:16
 */
?>
<script src="/components/ui/template/main/script.js"></script>
<link rel="stylesheet" href="/components/ui/template/main/css/style.css">
<div class="row">
    <div class="col-12">
        <img id="add-icon" class="ui-icon" data-toggle="modal" data-target="#exampleModalCenter" src="/components/ui/template/main/img/add-icon.svg">
    </div>
</div>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" id="data-to-push">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Добавление новой записи</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">Тип ВС: </div>
                            <div class="col-6"><input data-for="database" name="type_airplane" type="text" class="form-control" placeholder="Тип ВС"></div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">Номер Борта: </div>
                            <div class="col-6"><input data-for="database" name="board_number" type="text" class="form-control" placeholder="Номер Борта"></div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">Заводской Номер: </div>
                            <div class="col-6"><input data-for="database" name="factory_number" type="text" class="form-control" placeholder="Заводской Номер"></div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">Предприятие Изготовитель: </div>
                            <div class="col-6"><input data-for="database" name="manufacturer" type="text" class="form-control" placeholder="Предприятие Изготовитель"></div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">Дата Изготовления: </div>
                            <div class="col-6"><input data-for="database" name="date_manufacture" type="date" class="form-control" placeholder="Дата Изготовления"></div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">Дата Начала Изготовления: </div>
                            <div class="col-6"><input data-for="database" name="date_start_expluatation" type="date" class="form-control" placeholder="Дата Начала Изготовления"></div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">Собственник: </div>
                            <div class="col-6"><input data-for="database" name="owner" type="text" class="form-control" placeholder="Собственник"></div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">3D модель: </div>
                            <div class="col-6"><input data-for="database" name="3d_model" type="text" class="form-control" placeholder="3D модель"></div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">Комментарий: </div>
                            <div class="col-6"><textarea data-for="database" name="comment" rows="3" class="form-control" placeholder="Комментарий"></textarea></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="submit-form" data-dismiss="modal" class="btn btn-primary">Сохранить</button>
            </div>
        </div>
    </div>
</div>