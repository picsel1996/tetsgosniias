$(function () {
    var form = $('div#data-to-push');
    form.find('button#submit-form').on('click', function () {
        var dataSource = form.find('[data-for="database"]');
        var data = {
            'type_airplane':'ИЛ-76',
            'board_number':'IL76-234',
            'factory_number':'IL76-TD011',
            'manufacturer':'ОАО "СиАЗ"',
            'date_manufacture':'2019-06-09',
            'date_start_expluatation':'2019-06-09',
            'owner':'ОАО "Авиалинии Дагестана"',
            '3d_model':'ил-76',
            'comment':'Коммент'
        };
        $.each(dataSource, function (key, value) {
            if (value.value) {
                data[value.name] = value.value;
            }
        });
        console.log(data);
        sendAjax(data, 'airplanes', 'pushDataToDB');
    });
});

function sendAjax(data, component, method) {
    data['component'] = component;
    data['method'] = method;
    $.ajax({
        method: "POST",
        url: "/core/ajax.php",
        data: data
    }).done(function (msg) {
        console.log(msg);
        console.log($.parseJSON(msg));
        var value = $.parseJSON(msg);
            content = '<div class="col-12">\n' +
                '          <div class="row bg-light text-dark">\n' +
                '              <div class="col-1 border-right border-bottom">'+value['type_airplane']+'</div>\n' +
                '              <div class="col-1 border-right border-bottom">'+value['board_number']+'</div>\n' +
                '              <div class="col-1 border-right border-bottom">'+value['factory_number']+'</div>\n' +
                '              <div class="col-2 border-right border-bottom">'+value['manufacturer']+'</div>\n' +
                '              <div class="col-1 border-right border-bottom">'+value['date_manufacture']+'</div>\n' +
                '              <div class="col-1 border-right border-bottom">'+value['date_start_expluatation']+'</div>\n' +
                '              <div class="col-2 border-right border-bottom">'+value['owner']+'</div>\n' +
                '              <div class="col-1 border-right border-bottom">'+value['3d_model']+'</div>\n' +
                '              <div class="col-2 border-right border-bottom">'+value['comment']+'</div>\n' +
                '          </div>\n' +
                '      </div>';
        $("div#airplanes-data").append(content);
    });
}
