<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 23.06.2019
 * Time: 17:16
 */

class Ui extends \Core\MainComponent implements \Core\Component
{
    public function onLoadComponent()
    {
        $this->includeTemplate(__DIR__.'/template/main');
    }
}