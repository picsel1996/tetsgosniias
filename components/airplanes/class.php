<?php

/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 23.06.2019
 * Time: 19:43
 */

class Airplanes extends \Core\MainComponent implements \Core\Component
{
    public function onLoadComponent()
    {
        $this->arResult['AIRPLANES'] = $this->getAirplanes();
        $this->includeTemplate(__DIR__ . '/template/main');
    }

    public static function pushDataToDB($data)
    {
        require DIR_SITE.'core/orm/Airplanes.php';
        $airplane = new \Orm\Airplanes();
        $airplane->pushData($data);
        return $airplane->getData();
    }

    public function getAirplanes()
    {
        require DIR_SITE.'core/orm/Airplanes.php';
        $airplanes = new \Orm\Airplanes();
        return $airplanes->getData();
    }
}