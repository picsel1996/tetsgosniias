<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 23.06.2019
 * Time: 19:43
 */

$airplanes = $arResult['AIRPLANES'];
?>
<link rel="stylesheet" href="/components/airplanes/template/main/css/style.css?i=2">

<div class="col-10">
    <div class="row" id="airplanes-data">
        <div class="col-12 mt-2 mb-3">
            <img id="home-icon" class="home-icon" src="/components/airplanes/template/main/img/home-icon.svg">
            Воздушные суда
        </div>
        <div class="col-12">
            <div class="row">
                <div class="col-6">
                    <?php
                    require DIR_SITE."components/ui/class.php";
                    $ui = new Ui();
                    ?>
                </div>
            </div>
        </div>
        <p class="text-primary mb-0">Количество записей : <?=count($airplanes)?></p>
        <div class="col-12">
            <div class="row text-white bg-secondary">
                <div class="col-1 border-right border-bottom">Тип ВС</div>
                <div class="col-1 border-right border-bottom">Номер Борта</div>
                <div class="col-1 border-right border-bottom">Заводской Номер</div>
                <div class="col-2 border-right border-bottom">Предприятие Изготовитель</div>
                <div class="col-1 border-right border-bottom">Дата Изготовления</div>
                <div class="col-1 border-right border-bottom">Дата Начала Изготовления</div>
                <div class="col-2 border-right border-bottom">Собственник</div>
                <div class="col-1 border-right border-bottom">3D модель</div>
                <div class="col-2 border-right border-bottom">Комментарий</div>
            </div>
        </div>
        <?php
        foreach ($airplanes as $airplane) { ?>
            <div class="col-12">
                <div class="row bg-light text-dark">
                    <div class="col-1 border-right border-bottom"><?=$airplane['type_airplane']?></div>
                    <div class="col-1 border-right border-bottom"><?=$airplane['board_number']?></div>
                    <div class="col-1 border-right border-bottom"><?=$airplane['factory_number']?></div>
                    <div class="col-2 border-right border-bottom"><?=$airplane['manufacturer']?></div>
                    <div class="col-1 border-right border-bottom"><?=$airplane['date_manufacture']?></div>
                    <div class="col-1 border-right border-bottom"><?=$airplane['date_start_expluatation']?></div>
                    <div class="col-2 border-right border-bottom"><?=$airplane['owner']?></div>
                    <div class="col-1 border-right border-bottom"><?=$airplane['3d_model']?></div>
                    <div class="col-2 border-right border-bottom"><?=$airplane['comment']?></div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
