<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 22.06.2019
 * Time: 19:36
 */

//var_dump($arResult);

if (!$arResult['AUTHORIZED']) { ?>
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="align-items-center col-3">
                <form action="" id="authForm" method="post">
                    <div class="row">
                        <input type="hidden" name="login" value="Y">
                        <div class="form-group col-12">
                            <label for="inputLogin">Логин</label>
                            <input type="text" class="form-control" name="name" placeholder="Логин">
                            <small id="loginHelp" class="form-text text-muted">Уточните данные у администратора.</small>
                        </div>
                        <div class="form-group col-12">
                            <label for="inputPassword">Пароль</label>
                            <input type="password" class="form-control" name="password" placeholder="Пароль">
                        </div>
                        <button type="submit" class="btn btn-primary  col-12">Войти</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>