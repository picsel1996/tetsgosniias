<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 22.06.2019
 * Time: 19:43
 */


class Authorization extends \Core\MainComponent implements \Core\Component
{
    public function onLoadComponent()
    {
        if (key_exists('login', $_REQUEST) && $_REQUEST['login'] == 'Y') {
            $this->login($_REQUEST['name'], $_REQUEST['password']);
        }

        if (key_exists('logout', $_REQUEST) && $_REQUEST['logout'] == 'Y') {
            $this->logout();
        }

        global $userAuthorized;
        $userAuthorized = $this->arResult['AUTHORIZED'] = $this->isAuthorize();

        $this->includeTemplate(__DIR__.'/template/main');
    }

    public function isAuthorize():bool
    {
       if (is_array($_SESSION) && key_exists('AUTHORIZED', $_SESSION)) {
           return $_SESSION['AUTHORIZED'];
       }
        return false;
    }

    public function login($login = '', $password = '')
    {
        require DIR_SITE.'core/orm/User.php';
        $users = new \Orm\User();
        $users = $users->getData();
        $logins = array_column($users, 'login');
        $passwords = array_column($users, 'password');
        $key = in_array($login, $logins);
        if ($key && $passwords[$key] == $password) {
            $_SESSION['AUTHORIZED'] = true;
        } else {
            $_SESSION['AUTHORIZED'] = false;
        }
    }

    public function logout()
    {
        session_destroy();
        unset($_SESSION['AUTHORIZED']);
    }

}