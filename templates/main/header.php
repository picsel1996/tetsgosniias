<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 22.06.2019
 * Time: 19:22
 */
?>
    <html>
    <head>
        <script src="/templates/main/js/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
              integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
              crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
                integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
                crossorigin="anonymous"></script>
        <title>
            Тестовое задание для ГосНИИАС
        </title>
    </head>
<body>
<?php if ($userAuthorized) { ?>
    <div class="container-fluid">
        <div class="row border-bottom">
            <div class="col-2 border-right">
                <div class="row text-white bg-dark">
                    <div class="col-12">
                        <form action="" id="authForm" method="post">
                            <input type="hidden" name="logout" value="Y">
                            <button type="submit" class="btn btn-primary float-right">Выйти</button>
                        </form>
                    </div>
                    <div class="col-12">Скрыть</div>
                    <div class="col-12">Введение данных</div>
                    <div class="col-12">Анализ технического состояния</div>
                    <div class="col-12">Контроль выполнения</div>
                    <div class="col-12">Прогнозирование</div>
                    <div class="col-12">Мониторинг</div>
                    <div class="col-12">Режим БП</div>
                    <div class="col-12">Режим БПИнформационное взаимодействие</div>
                    <div class="col-12">Администрирование</div>
                    <div class="col-12">Справочники и классификаторы</div>
                </div>
            </div>
<?php } ?>