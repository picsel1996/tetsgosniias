<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 22.06.2019
 * Time: 21:47
 */
namespace Orm;

class User
{
    use TDataBase;
    private $table_name = 'users';
    public function getData()
    {
        return $this->fetchData($this->getSource($this->table_name));
    }
}