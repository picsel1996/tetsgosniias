<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 23.06.2019
 * Time: 14:20
 */

namespace Orm;

class Airplanes
{
    use TDataBase;
    private $table_name = 'airplanes';

    public function getData()
    {
        return $this->fetchData($this->getSource($this->table_name));
    }

    public function pushData($data = [])
    {
        $this->addData($this->table_name, $data);
    }
}