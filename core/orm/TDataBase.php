<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 22.06.2019
 * Time: 21:49
 */

namespace Orm;

trait TDataBase
{
    public function addData($table, $data = [])
    {
        global $link;
        foreach ($data as $key => $datum) {
            $column[] = $key;
            $values[] = '\''.$datum.'\'';
        }
        $query = 'INSERT INTO '.$table.' ('.implode(', ', $column).') VALUES ('.implode(', ', $values).')';
        return $link->query($query);
    }

    public function fetchData($source)
    {
        $data = [];
        while ($row = $source->fetch_assoc()) {
            array_push($data, $row);
        }
        return $data;
    }

    public function getSource($table)
    {
        global $link;
        return $link->query('SELECT * from '.$table);
    }
}