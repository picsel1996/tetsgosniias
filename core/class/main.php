<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 22.06.2019
 * Time: 19:54
 */

namespace Core;

class MainComponent implements Component
{
    public $arResult = [];
    public function __construct()
    {
        $this->onLoadComponent();
    }

    public function onLoadComponent()
    {
        return $params = false;
    }

    public function includeTemplate($template)
    {
        global $arResult;
        $arResult = $this->arResult;

        include $template.'/template.php';
    }
}