<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 22.06.2019
 * Time: 19:50
 */
namespace Core;

interface Component
{
    function onLoadComponent();
}