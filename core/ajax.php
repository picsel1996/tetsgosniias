<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 23.06.2019
 * Time: 21:50
 */

require 'require.php';

if (key_exists('component', $_POST) && key_exists('method', $_POST)) {
    $component = $_POST['component'];
    $method = $_POST['method'];
    require DIR_SITE . 'components/' . $component . '/class.php';
    unset($_POST['component']);
    unset($_POST['method']);
    $component = ucfirst($component);
    $data = $component::$method($_POST);
    echo json_encode(array_pop($data));
}
