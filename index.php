<?php
/**
 * Created by PhpStorm.
 * User: Илья
 * Date: 22.06.2019
 * Time: 19:05
 */

require 'core/require.php';
require 'templates/main/header.php';

if ($userAuthorized) {
    require DIR_SITE . "components/airplanes/class.php";
    $airplanes = new Airplanes();
}

require 'templates/main/footer.php';